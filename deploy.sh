# Step1 pull the changes 
# Bitbucket repo pull

# Step2 build the image
docker build -f ops/Dockerfile . -t hassansalem/express:$1

# Step3 push the image
docker push hassansalem/express:$1

# Step4 apply the deployment
kubectl apply -f ops/service.yml -f ops/ingress.yml

sed s/%version%/$1/g ops/deployment.yml | kubectl apply -f -
